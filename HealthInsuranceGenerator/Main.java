package HealthInsuranceGenerator;


import java.util.Scanner;
import static java.lang.Math.ceil;

public class Main {
    public static void main(String[] args) {
        InsuranceCalculator ic = new InsuranceCalculator();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your Name");
        String name = sc.next();

        System.out.println("Enter your Age");
        int age = sc.nextInt();

        System.out.println("Enter your Gender(M/F)");
        char gender = sc.next(".").charAt(0);

        System.out.println("Do you have any of these existing health conditions? ");
        int Conditions = preexistingConditions();

        System.out.println("Do you have any of these Good habits? ");
        int goodHabits = NumberOfGoodHabits();

        System.out.println("Do you have any of these Bad habits? ");
        int badHabits = NumberOfBadHabits();

       double totalAmount = ceil(ic.calculate(age,gender,Conditions, goodHabits,badHabits));
        System.out.println("Health Insurance Premium for Mr " +name+ ":" + totalAmount);
    }


    public static int preexistingConditions() {
        int count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Do You have Hypertension(Y/N)");
        char hp = sc.next(".").charAt(0);
        System.out.println("Do You have BloodPressure? (Y/N)");
        char bp = sc.next(".").charAt(0);
        System.out.println("Do You have Blood Sugar?(Y/N)");
        char sugar = sc.next(".").charAt(0);
        System.out.println("Are you OverWeight?(Y/N)");
        char overweight = sc.next(".").charAt(0);
        char a[] = {hp, bp, sugar, overweight};
        for (int i = 0; i < a.length; i++) {
            if (a[i] == 'Y') {
                count++;
            }
        }
        return (count);
    }

    public static int NumberOfGoodHabits() {
        int count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Do You do Daily Exercises (Y/N)");
        char exercise = sc.next(".").charAt(0);
        if (exercise == 'Y') {
            count++;
        } else {
            return (count);
        }
        return count;
    }

    public static int NumberOfBadHabits() {
        int count = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Do You Smoke(Y/N)");
        char smoke = sc.next(".").charAt(0);
        System.out.println("Do You Consume Alcohol? (Y/N)");
        char alcohol = sc.next(".").charAt(0);
        System.out.println("Do You take Drugs?(Y/N)");
        char drugs = sc.next(".").charAt(0);
        char a[] = {smoke, alcohol, drugs};
        for (int i = 0; i < a.length; i++) {
            if (a[i] == 'Y') {
                count++;
            } else ;

        }
        return (count);
    }
}



